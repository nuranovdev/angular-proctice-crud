import { Component, Input } from '@angular/core';
import { IUser } from '../../models/user';

@Component({
  selector: 'app-user',
  standalone: true,
  imports: [],
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent {

  @Input() user: IUser

  details: boolean = false
}
