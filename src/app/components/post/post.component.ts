import { Component, Input } from '@angular/core';
import { IPost } from '../../models/post';

@Component({
  selector: 'app-post',
  standalone: true,
  imports: [],
  templateUrl: './post.component.html',
  styleUrl: './post.component.css'
})
export class PostComponent {

  @Input() post: IPost

  details: boolean = false
}
