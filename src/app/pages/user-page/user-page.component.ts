import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-user-page',
  standalone: true,
  imports: [],
  templateUrl: './user-page.component.html',
  styleUrl: './user-page.component.css'
})
export class UserPageComponent {
  loading: boolean = false

  constructor(
    public userService: UserService
  ) {
    
  }


  ngOnInit(): void {
    this.userService.getAll().subscribe( () => 
    this.loading = true
    )      
  }
}
