import { Component, OnInit } from '@angular/core';
import { PostService } from '../../services/post.service';
import { HttpClientModule } from '@angular/common/http';

@Component({
  selector: 'app-post-page',
  standalone: true,
  imports: [],
  templateUrl: './post-page.component.html',
  styleUrl: './post-page.component.css'
})
export class PostPageComponent {
  loading: boolean = false;
    
  constructor (
    public postService: PostService
  ) {}

  
  ngOnInit(): void {    
    this.postService.getAll().subscribe(()=>
    this.loading = true
    )    
  }
}
