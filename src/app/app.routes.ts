import { Routes } from '@angular/router';
import { PostPageComponent } from './pages/post-page/post-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';
import { PostService } from './services/post.service';
import { UserService } from './services/user.service';

export const routes: Routes = [
    {path:"", component: PostPageComponent, providers:[PostService]},
    {path:"users", component: UserPageComponent, providers:[UserService]}
];
