export interface IPost {
    id: number,
    owner: number,
    title: string,
    description: string,
    image: string 
}