import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IUser } from '../models/user';
import { Observable, delay, retry, tap } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) {}

  getAll(): Observable<IUser[]> {
    return this.http.get<IUser[]>("http://127.0.0.1:8000/api/accounts/").pipe(
      delay(2000),
      retry(2)
    )
  }

  getOne(id: number): Observable<IUser> {
    return this.http.get<IUser>(`http://127.0.0.1:8000/api/accounts/${id}`)
  }
  
}
