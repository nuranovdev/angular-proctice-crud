import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { IPost } from '../models/post';
import { Observable, delay, retry } from 'rxjs';
import { PostPageComponent } from '../pages/post-page/post-page.component';

@Injectable({
  providedIn: "root",
})
export class PostService {

  constructor(
    private http: HttpClient
  ) {}

  getAll(): Observable<IPost[]> {
    return this.http.get<IPost[]>("http://127.0.0.1:8000/api/posts/",{
      headers: {
        "Access-Control-Allow-Origin" : "http://localhost:4200"
      }}).pipe(
      delay(2000),
      retry(2)
    )
  }
  
  getOne(id: number): Observable<IPost> {
    return this.http.get<IPost>(`127.0.0.1:8000/api/posts/${id}`)
  }
}
